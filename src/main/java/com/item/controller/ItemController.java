package com.item.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.item.entity.Item;
import com.item.service.ItemService;

@RestController
public class ItemController {

	@Autowired
	private ItemService itemService;

	@GetMapping("items/{id}")
	public ResponseEntity<Item> getItemById(@PathVariable Integer id) {
		ResponseEntity<Item> response;
		return response = new ResponseEntity<Item>(itemService.getItembyProdCode(id), HttpStatus.OK);

	}

}
