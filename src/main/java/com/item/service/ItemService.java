package com.item.service;

import org.springframework.stereotype.Service;

import com.item.entity.Item;

@Service
public interface ItemService {

	public Item getItembyProdCode(Integer id);
}
