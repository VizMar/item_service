package com.item.service;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.item.entity.Item;
import com.item.exception.ProductNotFoundException;
import com.item.repository.ItemRepository;

@Service
public class ItemServiceImpl implements ItemService {

	@Autowired
	private ItemRepository itemRepository;

	@Override
	public Item getItembyProdCode(Integer id) {

		Optional<Item> result = itemRepository.findById(id);
		if (!result.isPresent())
			throw new ProductNotFoundException("Id Not found");
		return result.get();
	}

}
