package com.item.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;

import com.item.entity.Item;

@RestControllerAdvice
public class ProductNotFoundExceptionHandler {

	@ExceptionHandler(value = {ProductNotFoundException.class })
	protected ResponseEntity<Object> handleConflict(RuntimeException ex, WebRequest request) {
		
		return new ResponseEntity<>("Id does not exist", HttpStatus.NOT_FOUND);
	}
}
